all:
	pdflatex openmp-openacc
	bibtex openmp-openacc
	pdflatex openmp-openacc
	pdflatex openmp-openacc
clean:
	rm *.log *.aux *.blg *.bbl
veryclean:
	rm *.pdf
