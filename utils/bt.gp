#set xlabel "Sampled Data Size"
set ylabel "Time (sec)"
set grid
set key right top
set style data histogram
set style fill pattern 9
set yrange [0:1000]
#set style fill solid
#set term pdfcairo
set term pdf
set output "nas.pdf"
plot "nas.dat" using 2:xticlabels(1) title "CLASS A", \
'' using 3 title "CLASS B", \
'' using 4 title "CLASS C"
set output
set term wxt


set xlabel "Data Size"
set ylabel "Time (sec)"
set grid
set key right top
set style data histogram
set style fill pattern 9
set yrange [0:100]
#set style fill solid
#set term pdfcairo
set term pdf
set output "heat.pdf"
plot "heat.dat" using 2:xticlabels(1) title "OpenUH", \
'' using 3 title "LLVM"
set output
set term wxt

set xlabel "Data Size"
set ylabel "Time (sec)"
set grid
set key right top
set style data histogram
set style fill pattern 9
set yrange [0:350]
#set style fill solid
#set term pdfcairo
set term pdf
set output "gauss.pdf"
plot "gauss.dat" using 2:xticlabels(1) title "OpenUH", \
'' using 3 title "LLVM"
set output
set term wxt

set xlabel "Data Size"
set ylabel "Time (sec)"
set grid
set key right top
set style data histogram
set style fill pattern 9
set yrange [0:80]
#set style fill solid
#set term pdfcairo
set term pdf
set output "laplacian.pdf"
plot "laplacian.dat" using 2:xticlabels(1) title "OpenUH", \
'' using 3 title "LLVM"
set output
set term wxt

set xlabel "Data Size"
set ylabel "Time (sec)"
set grid
set key left top
set style data histogram
set style fill pattern 9
set yrange [0:1000]
#set style fill solid
#set term pdfcairo
set term pdf
set output "matmult.pdf"
plot "matmult.dat" using 2:xticlabels(1) title "OpenUH", \
'' using 3 title "LLVM"
set output
set term wxt


#set xlabel "Data Size"
set ylabel "Time (sec)"
set grid
set key left top
set style data histogram
set style fill pattern 9
set yrange [0:120]
#set style fill solid
#set term pdfcairo
set term pdf
set output "nas_cache_b.pdf"
plot "nas_cache_b.dat" using 2:xticlabels(1) title "GPU", \
'' using 3 title "CPU (16 threads)"
set output
set term wxt


#set xlabel "Data Size"
set ylabel "Time (sec)"
#set logscale y
set grid
set key left top
set style data histogram
set style fill pattern 9
set yrange [0:600]
#set style fill solid
#set term pdfcairo
set term pdf
set output "nas_cache_c.pdf"
plot "nas_cache_c.dat" using 2:xticlabels(1) title "GPU", \
'' using 3 title "CPU (16 threads)"
set output
set term wxt
